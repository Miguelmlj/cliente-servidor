package otroclientecopia2;
import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OtroClienteCopia2 {

    
    public static void main(String[] args){
       
        //ERROR POR FALTA DE ARGUMENTOS EN LINEA DE COMANDO--------
        if(args.length == 0){
                System.out.println("Te faltó darme el ip y el nombre de archivo");
                System.exit(0);
        }else if(args.length == 1){
            
            if(args[0].equalsIgnoreCase("127.0.0.1")){
                System.out.println("Te faltó darme nombre de archivo");
                System.exit(0);
            }else{
                System.out.println("Te faltó darme ip");
                System.exit(0);
            }
            
        }
        //--------------------------------------------------------
        
        try {
            
            String servidor = args[0];
            String nombreArchivo = args[1];
           
            Socket socket = new Socket(servidor,8000);
            
            File archivo = new File(nombreArchivo);
            
                //Error si el archivo no se encuentra en la carpeta
                if(!archivo.exists()){
                    
                    System.out.println("El archivo que intentas enviar no existe");
                    System.exit(0);
                
                }else{
            
                    PrintWriter boca = new PrintWriter(socket.getOutputStream(),true);
                    
                    boca.println(nombreArchivo);
                    
                    InputStream entrada = new FileInputStream(archivo); //se lee inf. de archivo
                    
                    DataOutputStream salida = new DataOutputStream(socket.getOutputStream()); //se envía File a través de socket
                    
                    entrada.transferTo(salida); //todo lo que hay en entrada transefirlo a la salida
                    
                    socket.close();
            }
            
            }catch(IOException e){
            
                if(e.getMessage().equalsIgnoreCase("Connection refused: connect"))
                    System.out.println("No me pude conectar con el servidor");
            
            }
    }
    
}

            /*ERRORES
            -Error por falta de argumento ip
            -Error por falta de argumento archivo
            -Error por servidor no conectado
            -Error por no encotrar el archivo a enviar

             */