
package otroservidorcopia2;
import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OtroServidorCopia2 {

    
    public static void main(String[] args) {
        
        try {
            ServerSocket serverSocket = new ServerSocket(8000);
            
            Socket socket = serverSocket.accept();
            
            BufferedReader oido = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            
            String nombreArchivo;
            
            nombreArchivo = oido.readLine();
            
            System.out.println(nombreArchivo);
            
            File archivo = new File(nombreArchivo);
            
            OutputStream salida = new FileOutputStream(archivo);
            
            DataInputStream recepcion = new DataInputStream(socket.getInputStream()); //recibe el File que envio cliente
            
            recepcion.transferTo(salida);
            
            socket.close();
        
        } catch (IOException ex) {
            System.out.println("IOException: " + ex.getMessage());
            
        }
    }
    
}
